const $ = (selector) => document.querySelector(selector)

// front end UI elements grrabbed jquery style
const uglyDogsPage = $('#ugly-dogs')
const dogListPage = $('#dogs-list')
const loading = $('#loading')
const content = $('#content')
const filter = $('#filter')
const dog1Element = $('#dog1')
const dog2Element = $('#dog2')
const pug = $('#pug')
const chastize = $('#chastize')

// variables for for checking ugliest
const pairings = []
const scoresMap = {}
const breedsArray = []
let loadingCount = 0

// Task One (check the console)
function generateRandomInts (num, max, min) {
  const arrayInts = []
  for (let i = 0; i < num; i++) {
    arrayInts.push(Math.floor(Math.random() * (max - min) + min))
  }
  arrayInts.sort((a, b) => b - a)
  const result = arrayInts.filter((val) => val % 15 === 0)
  return result.length
}

console.log('Your random ints')
console.log(generateRandomInts(16, 1200, -1000))

// Can't reliably make calls from client side so this is a workaround that may unfortunately slow things down a hair
const corsPrefix = 'https://cors-anywhere.herokuapp.com/'
async function fetchUrl (url) {
  const response = await fetch(corsPrefix + url)
  return response.json()
}

async function initialize () {
  // grab dogs and handle sub-breeds
  toggleLoading(true)
  const response = await fetchUrl('https://dog.ceo/api/breeds/list/all')
  for (const breed in response.message) {
    if (response.message[breed].length > 0) {
      for (const subBreed of response.message[breed]) {
        breedsArray.push(`${breed}-${subBreed}`)
      }
    } else {
      breedsArray.push(breed)
    }
  }
  // create scores map and head to head pairings
  for (let i = 0; i < breedsArray.length; i++) {
    scoresMap[breedsArray[i]] = 0
    for (let j = i + 1; j < breedsArray.length; j++) {
      pairings.push([breedsArray[i], breedsArray[j]])
    }
  }

  shuffle(pairings)
  setUglyDogImages()
  const displayArray = structuredClone(breedsArray)
  displayBreedsList(displayArray)
  toggleLoading(false)
}

function shuffle (array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]]
  }
}

async function setUglyDogImages () {
  const dog1Image = await returnDogImages(pairings[0][0])
  const dog2Image = await returnDogImages(pairings[0][1])
  dog1Element.setAttribute('src', dog1Image)
  dog2Element.setAttribute('src', dog2Image)
}

async function returnDogImages (breed) {
  breed = handleSubBreed(breed)
  const response = await fetchUrl(`https://dog.ceo/api/breed/${breed}/images/random`)
  return response.message
}

function handleSubBreed (breed) {
  const split = breed.split('-')
  if (split.length > 1) {
    breed = split[0] + '/' + split[1]
  }
  return breed
}

function displayBreedsList (displayArray) {
  const breedList = $('.breedlist')
  breedList.innerHTML = displayArray
    .map(dogs => `
        <li class="click-pointer">${dogs}</li>`)
    .join('')
  const links = $('.breedlist').getElementsByTagName('li')
  for (const li of links) {
    li.onclick = changePageToDogList
  }
}

function changePageToDogList (event) {
  toggleLoading(true)
  uglyDogsPage.style.display = 'none'
  renderDogsList(event.srcElement.innerHTML)
  toggleLoading(false)
}

dog1Element.onclick = chooseUgliest
dog2Element.onclick = chooseUgliest

function chooseUgliest (event) {
  toggleLoading(true)
  const ugliest = event.srcElement.src.split('/')[4]
  scoresMap[ugliest]++
  updateUglyDogRankings(scoresMap)
  pairings.shift()
  setUglyDogImages()
  toggleLoading(false)
}

function updateUglyDogRankings (scoresMap) {
  const scoresArray = Object.keys(scoresMap)
    .map(key => ({ name: key, score: scoresMap[key] }))
    .filter((item) => item.score > 0)
  scoresArray.sort((a, b) => {
    return b.score - a.score
  })
  const leaderboard = scoresArray.splice(0, 10)
  $('#ugliest-rankings').innerHTML = leaderboard
    .map(dogs => `<li style="margin: 10px 0px;"><strong>${dogs.score}</strong> ${dogs.name}</li>`)
    .join('')
}

loading.onclick = changePageUglyDogs
function changePageUglyDogs () {
  dogListPage.style.display = 'none'
  toggleLoading(true)
  uglyDogsPage.style.display = 'unset'
  toggleLoading(false)
}

async function renderDogsList (breed) {
  breed = handleSubBreed(breed)
  const response = await fetchUrl(`https://dog.ceo/api/breed/${breed}/images/random/10`)
  const dogsList = response.message
  $('#dogs-list').innerHTML = dogsList
    .map(dogs => `
        <div class="dog-wrapper">
            <img class="dog" src="${dogs}">
        </div>`)
    .join('')
  dogListPage.style.display = 'flex'
}

function toggleLoading (state) {
  const chastizeArray = [
    'You know better.',
    'What would mom think?',
    'You were raised better.',
    'How could you?',
    "I'm hurt.",
    'Please.',
    'Why?'
  ]
  loadingCount++
  if (loadingCount > 2) {
    const randomIndex = Math.floor(Math.random() * 4 + 1)
    pug.setAttribute('src', `./assets/images/${randomIndex}.png`)
    const randomChastize = Math.floor(Math.random() * chastizeArray.length)
    chastize.innerHTML = chastizeArray[randomChastize]
  }
  if (loadingCount === 40) {
    const sarah = new Audio('./assets/sounds/angel.mp3')
    sarah.play()
  }
  if (state) {
    loading.style.display = 'flex'
    content.style.display = 'none'
    const randomSound = Math.floor(Math.random() * 10 + 1)
    const audio = new Audio(`./assets/sounds/${randomSound}.mp3`)
    audio.play()
  } else {
    setTimeout(() => {
      loading.style.display = 'none'
      content.style.display = 'unset'
    }, 2000)
  }
}

// filter list
filter.addEventListener('keydown', function (event) {
  // reset array
  if (event.srcElement.value.length > 0) {
    const displayArray = breedsArray
    displayBreedsList(displayArray)
  }
  if (event.key === 'Enter') {
    const displayArray = breedsArray.filter(str => str.includes(event.srcElement.value))
    displayBreedsList(displayArray)
  }
})

initialize()
