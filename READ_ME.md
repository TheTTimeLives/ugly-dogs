# How to Run

Was done in Vanilla JS/HTML. I didn't see any request for Angular/React. Would have served front end from Express as well but didn't see a request for that either. Hence needing the CORs workarounds below.

BEFORE RUNNING:
Hit the website below to allow CORS through the CORS proxy site.

[CORS Anywhere](https://cors-anywhere.herokuapp.com/)

(The link may also be dynamic so if the page isn't running, check the console and click the link that this website provides you)

Additionally, there are several Chrome plugins to turn off CORS in your browser, or you can run your browser with flags to turn off CORS on launch.

Afterwards, just run the HTML file locally within the unzipped dir.

The First Task was completed in the console and the assignment is the Second Task. I believe I completed most of the optional side requests as well.


